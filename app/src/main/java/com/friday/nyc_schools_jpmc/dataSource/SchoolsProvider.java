package com.friday.nyc_schools_jpmc.dataSource;

import com.friday.nyc_schools_jpmc.dataModel.SchoolItem;
import com.friday.nyc_schools_jpmc.dataModel.SchoolListItem;
import com.friday.nyc_schools_jpmc.mock.MyBodyFactory;

import java.util.List;

import co.infinum.retromock.meta.Mock;
import co.infinum.retromock.meta.MockResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolsProvider {

    @Mock
    @MockResponse(body = "list.json")
    @GET("97mf-9njv.json")
    Call<List<SchoolListItem>> getSchools();


    @Mock
    @MockResponse(body = "details.json")
    @GET("f9bf-2cp4.json")
    Call<List<SchoolItem>> getSchoolDetail(@Query("dbn") String id);
}
