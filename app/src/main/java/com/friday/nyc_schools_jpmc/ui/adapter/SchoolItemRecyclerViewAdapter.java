package com.friday.nyc_schools_jpmc.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.friday.nyc_schools_jpmc.dataModel.SchoolListItem;
import com.friday.nyc_schools_jpmc.databinding.SchoolItemBinding;

import java.util.List;

public class SchoolItemRecyclerViewAdapter extends RecyclerView.Adapter<SchoolItemRecyclerViewAdapter.SchoolListViewHolder> {

    private final Context mContext;
    private final List<SchoolListItem> mValues;
    private OnClickListener itemClickListener = null;

    public SchoolItemRecyclerViewAdapter(List<SchoolListItem> items, Context context, OnClickListener listener) {
        mValues = items;
        mContext = context;
        itemClickListener = listener;
    }

    @Override
    public SchoolListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new SchoolListViewHolder(SchoolItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), mContext);

    }

    @Override
    public void onBindViewHolder(final SchoolListViewHolder holder, int position) {
        //initialize and set holder attributes and actions
        holder.setValues(mValues.get(position));
        holder.itemView.setOnClickListener(v -> itemClickListener.onClick(position, mValues.get(position)));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface OnClickListener {
        void onClick(int position, SchoolListItem item);
    }

    public class SchoolListViewHolder extends RecyclerView.ViewHolder {
        public TextView schoolName;
        public TextView schoolStudents;
        public TextView schoolWebsite;
        public TextView schoolAddressLine1;
        public TextView schoolAddressLine2;
        public ImageView studentCountIcon;
        public ImageView websiteIcon;
        public ImageView studentAddressIcon;
        public SchoolListItem mItem;
        public Context context;

        public SchoolListViewHolder(SchoolItemBinding binding, Context mContext) {
            super(binding.getRoot());
            schoolName = binding.schoolName;
            schoolStudents = binding.schoolStudents;
            schoolAddressLine1 = binding.schoolAddressLine1;
            schoolAddressLine2 = binding.schoolAddressLine2;
            schoolWebsite = binding.schoolWebsite;
            studentCountIcon = binding.studentCountIcon;
            websiteIcon = binding.websiteIcon;
            studentAddressIcon = binding.studentAddressIcon;
            this.context = mContext;
        }

        public void setValues(SchoolListItem item) {
            schoolName.setText(item.getSchoolName());
            if (!item.getTotalStudents().isEmpty())
                schoolStudents.setText(String.format("%s students", item.getTotalStudents()));
            else {
                schoolStudents.setVisibility(View.INVISIBLE);
                studentCountIcon.setVisibility(View.INVISIBLE);
            }
            if (!item.getWebsite().isEmpty())
                schoolWebsite.setText(item.getWebsite());
            else {
                schoolWebsite.setVisibility(View.INVISIBLE);
                websiteIcon.setVisibility(View.INVISIBLE);
            }
            if (item.getPrimaryAddressLine1().isEmpty() && item.getCity().isEmpty() && item.getStateCode().isEmpty() && item.getZip().isEmpty()) {
                schoolAddressLine1.setVisibility(View.INVISIBLE);
                schoolAddressLine2.setVisibility(View.INVISIBLE);
                studentAddressIcon.setVisibility(View.INVISIBLE);
            }
            schoolAddressLine1.setText(item.getPrimaryAddressLine1());
            schoolAddressLine2.setText(String.format("%s, %s %s", item.getCity(), item.getStateCode(), item.getZip()));
            mItem = item;
        }

        @Override
        public String toString() {
            return "ViewHolder{" +
                    "schoolAddressLine1=" + schoolAddressLine1.getText() +
                    ", schoolAddressLine2=" + schoolAddressLine2.getText() +
                    ", schoolWebsite=" + schoolWebsite.getText() +
                    ", schoolStudents=" + schoolStudents.getText() +
                    ", schoolName=" + schoolName.getText() +
                    '}';
        }
    }
}