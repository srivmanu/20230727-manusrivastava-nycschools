package com.friday.nyc_schools_jpmc.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friday.nyc_schools_jpmc.BuildConfig;
import com.friday.nyc_schools_jpmc.R;
import com.friday.nyc_schools_jpmc.SchoolListActivity;
import com.friday.nyc_schools_jpmc.ui.adapter.SchoolItemRecyclerViewAdapter;
import com.friday.nyc_schools_jpmc.dataModel.SchoolListItem;
import com.friday.nyc_schools_jpmc.dataSource.SchoolsProvider;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 */
public class SchoolItemFragment extends Fragment {
    String TAG = "6268";
    private SchoolsProvider provider = null;
    private ProgressBar progress;
    private View recycler_view;
    private List<SchoolListItem> items;
    private SchoolItemRecyclerViewAdapter adapter;
    private View alert;
    private Context parentActivity;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SchoolItemFragment() {
    }

    public SchoolItemFragment(SchoolsProvider provider,Context parentActivity) {
        this.provider = provider;
        this.parentActivity = parentActivity;
    }

    public static SchoolItemFragment newInstance(SchoolsProvider provider, Context parentActivity) {
        return new SchoolItemFragment(provider,parentActivity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_item_list, container, false);
        //initialize views
        progress = view.findViewById(R.id.list_progress);
        recycler_view = view.findViewById(R.id.list);
        alert = view.findViewById(R.id.list_alert);
        //display the progress view
        showProgressView();

        //init the items list which will be updated with items when list is retrieved.
        items = new ArrayList<>();

        if (recycler_view instanceof RecyclerView) {
            Context context = recycler_view.getContext();
            RecyclerView recyclerView = (RecyclerView) recycler_view;

            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            //define item view click listener for items in list
            SchoolItemRecyclerViewAdapter.OnClickListener itemClickListener = new SchoolItemRecyclerViewAdapter.OnClickListener() {
                @Override
                public void onClick(int position, SchoolListItem item) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "onClick: " + item.getDbn());
                    }
                    if (!item.getDbn().isEmpty()) {
                        //replace list fragment with details fragment, with back stack set.
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_layout, SchoolDetailFragment.newInstance(provider, item,parentActivity))
                                .addToBackStack(null)
                                .commit();
                    }
                }
            };

            //define and set adapter.
            adapter = new SchoolItemRecyclerViewAdapter(items, context, itemClickListener);
            recyclerView.setAdapter(adapter);

            // make network call and show list of schools.
            if (provider != null) {
                Call<List<SchoolListItem>> schoolsListCall = provider.getSchools();
                schoolsListCall.enqueue(new Callback<List<SchoolListItem>>() {
                    @Override
                    public void onResponse(Call<List<SchoolListItem>> call, Response<List<SchoolListItem>> response) {
                        hideProgressView();
                        List<SchoolListItem> schoolsList = response.body();
                        items.clear();
                        if (schoolsList != null) {
                            items.addAll(schoolsList);
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SchoolListItem>> call, Throwable t) {
                        //show error dialog
                        call.cancel();
                        hideProgressView();
                        showError();
                    }
                });
            }
        }

        return view;
    }


    private void showError() {
        alert.setVisibility(View.VISIBLE);
        progress.setVisibility(View.INVISIBLE);
        recycler_view.setVisibility(View.INVISIBLE);
    }

    private void hideProgressView() {
        if (progress != null && progress.getVisibility() == View.VISIBLE) {
            progress.setVisibility(View.INVISIBLE);
            recycler_view.setVisibility(View.VISIBLE);
            alert.setVisibility(View.INVISIBLE);
        }
    }

    private void showProgressView() {
        if (progress != null && progress.getVisibility() == View.INVISIBLE) {
            progress.setVisibility(View.VISIBLE);
            recycler_view.setVisibility(View.INVISIBLE);
            alert.setVisibility(View.INVISIBLE);
        }
    }
}