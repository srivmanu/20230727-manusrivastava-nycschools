package com.friday.nyc_schools_jpmc.ui.vm;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModel;

import com.friday.nyc_schools_jpmc.R;
import com.friday.nyc_schools_jpmc.dataModel.SchoolItem;
import com.friday.nyc_schools_jpmc.dataModel.SchoolListItem;

public class SchoolDetailViewModel extends ViewModel {
    private final Context context;
    private final ImageView phone;
    private final ImageView website;
    private final ImageView email;
    private final ImageView location;
    private final TextView name;
    private final TextView addressL1;
    private final TextView addressL2;
    private final TextView sat_count;
    private final TextView sat_crit_read;
    private final TextView sat_math;
    private final TextView sat_writing;

    public SchoolDetailViewModel(View view, Context context) {
        this.context = context;
        phone = view.findViewById(R.id.detail_phone);
        website = view.findViewById(R.id.detail_website);
        email = view.findViewById(R.id.detail_email);
        location = view.findViewById(R.id.detail_location);

        addressL1 = view.findViewById(R.id.detail_addressL1);
        addressL2 = view.findViewById(R.id.detail_addressL2);
        sat_count = view.findViewById(R.id.detail_sat_count);
        sat_crit_read = view.findViewById(R.id.detail_sat_crit_read);
        sat_math = view.findViewById(R.id.detail_sat_math);
        sat_writing = view.findViewById(R.id.detail_sat_writing);
        name = view.findViewById(R.id.detail_name);
    }

    public void setViews(SchoolListItem listItem, SchoolItem item) {
        this.name.setText(listItem.getSchoolName());
        this.addressL1.setText(listItem.getPrimaryAddressLine1());
        this.addressL2.setText(String.format("%s, %s %s", listItem.getCity(), listItem.getStateCode(), listItem.getZip()));
        this.sat_count.setText(item.getNumOfSatTestTakers());
        this.sat_crit_read.setText(item.getSatCriticalReadingAvgScore());
        this.sat_math.setText(item.getSatMathAvgScore());
        this.sat_writing.setText(item.getSatWritingAvgScore());

        website.setOnClickListener(v -> {
            String url = listItem.getWebsite();
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));

            Intent chooser = Intent.createChooser(i, "Open with");
            context.startActivity(chooser);

        });

        phone.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + listItem.getPhoneNumber()));
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            }

        });

        email.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_EMAIL, listItem.getSchoolEmail());
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            }

        });

        location.setOnClickListener(v -> {
            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + listItem.getPrimaryAddressLine1() + " , "+ listItem.getCity() + " , " + listItem.getStateCode());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            Intent chooser = Intent.createChooser(mapIntent, "Open with");
            context.startActivity(chooser);
        });
    }
}