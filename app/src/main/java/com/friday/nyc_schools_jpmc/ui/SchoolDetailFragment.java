package com.friday.nyc_schools_jpmc.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.friday.nyc_schools_jpmc.BuildConfig;
import com.friday.nyc_schools_jpmc.R;
import com.friday.nyc_schools_jpmc.ui.vm.SchoolDetailViewModel;
import com.friday.nyc_schools_jpmc.dataModel.SchoolItem;
import com.friday.nyc_schools_jpmc.dataModel.SchoolListItem;
import com.friday.nyc_schools_jpmc.dataSource.SchoolsProvider;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolDetailFragment extends Fragment {
    String TAG = "6268";
    private final SchoolListItem item;
    private final SchoolsProvider provider;
    private SchoolDetailViewModel mViewModel;
    private SchoolItem schoolItem;
    private View progress;
    private View content;
    private View alert;
    private TextView alertText;
    private Context parentActivity;

    public SchoolDetailFragment(SchoolsProvider provider, SchoolListItem item, Context parentActivity) {
        this.provider = provider;
        this.item = item;
        this.parentActivity = parentActivity;
    }

    public static SchoolDetailFragment newInstance(SchoolsProvider provider, SchoolListItem item, Context parentActivity) {
        return new SchoolDetailFragment(provider, item,parentActivity);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View inflate = inflater.inflate(R.layout.fragment_school_detail, container, false);
        progress = inflate.findViewById(R.id.detail_progress);
        content = inflate.findViewById(R.id.content);
        alert = inflate.findViewById(R.id.detail_alert);
        alertText = inflate.findViewById(R.id.detail_alert_text);
        showProgressView();
        if (!this.item.getDbn().isEmpty()) {
            Call<List<SchoolItem>> schoolDetailCall = provider.getSchoolDetail(this.item.getDbn());
            schoolDetailCall.enqueue(new Callback<List<SchoolItem>>() {
                @Override
                public void onResponse(Call<List<SchoolItem>> call, Response<List<SchoolItem>> response) {
                    if (response.body().size() > 0)
                        updateView(inflate, response.body().get(0));
                    else {
                        showError("");
                    }
                }

                @Override
                public void onFailure(Call<List<SchoolItem>> call, Throwable t) {
                    //show alert of error
                    showError(t.getLocalizedMessage());
                }
            });
        }
        return inflate;
    }

    public void showProgressView() {
        progress.setVisibility(View.VISIBLE);
        content.setVisibility(View.INVISIBLE);
        alert.setVisibility(View.INVISIBLE);
    }

    public void showError(String localizedMessage) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "showError() called with: localizedMessage = [" + localizedMessage + "]");
        }
        progress.setVisibility(View.INVISIBLE);
        content.setVisibility(View.INVISIBLE);
        alert.setVisibility(View.VISIBLE);
        alertText.setText(alertText.getText() + "\n" + localizedMessage);
    }

    public void hideProgressView() {
        progress.setVisibility(View.INVISIBLE);
        content.setVisibility(View.VISIBLE);
        alert.setVisibility(View.INVISIBLE);
    }

    public void updateView(View inflate, SchoolItem schoolItem) {
        hideProgressView();
        mViewModel = new SchoolDetailViewModel(inflate, parentActivity);
        mViewModel.setViews(this.item, schoolItem);
    }


}