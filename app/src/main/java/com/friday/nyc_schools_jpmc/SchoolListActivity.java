package com.friday.nyc_schools_jpmc;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.friday.nyc_schools_jpmc.dataSource.SchoolsProvider;
import com.friday.nyc_schools_jpmc.ui.SchoolItemFragment;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolListActivity extends AppCompatActivity {

    private final String TAG = "6268";
    private final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);
        SchoolsProvider schoolProvider = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SchoolsProvider.class);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_layout, SchoolItemFragment.newInstance(schoolProvider,this)).commit();

        //make details fragment

    }

    /**
     * Call<List<SchoolListItem>> schoolListCall = schoolProvider.getSchools();
     *         if (BuildConfig.DEBUG) {
     *             Log.d(TAG, "onCreate: CALLED");
     *         }
     *         schoolListCall.enqueue(new Callback<List<SchoolListItem>>() {
     *             @Override
     *             public void onResponse(Call<List<SchoolListItem>> call, Response<List<SchoolListItem>> response) {
     *                 List<SchoolListItem> list = response.body();
     *                 if (BuildConfig.DEBUG) {
     *                     Log.d(TAG, "onCreate: " + list.get(0).toString());
     *                 }
     *                 //populate RecyclerView
     *             }
     *
     *             @Override
     *             public void onFailure(Call<List<SchoolListItem>> call, Throwable t) {
     *                 if (BuildConfig.DEBUG) {
     *                     Log.d(TAG, "onFailure: FAILED " + t.getLocalizedMessage());
     *                 }
     *                 call.cancel();
     *                 //Display Error Alert
     *             }
     *         });
     *
     *         Call<List<SchoolItem>> x = schoolProvider.getSchoolDetail("01M448");
     *         x.enqueue(new Callback<List<SchoolItem>>() {
     *             @Override
     *             public void onResponse(Call<List<SchoolItem>> call, Response<List<SchoolItem>> response) {
     *                 List<SchoolItem> school = response.body();
     *                 if (BuildConfig.DEBUG) {
     *                     Log.d(TAG, "onResponse: " + school.get(0).toString());
     *                 }
     *                 //Display Detail in fragment
     *             }
     *
     *             @Override
     *             public void onFailure(Call<List<SchoolItem>> call, Throwable t) {
     *                 if (BuildConfig.DEBUG) {
     *                     Log.d(TAG, "onFailure: FAILED " + t.getLocalizedMessage());
     *                 }
     *                 call.cancel();
     *                 //Display Error Alert
     *             }
     *         });
     */
}