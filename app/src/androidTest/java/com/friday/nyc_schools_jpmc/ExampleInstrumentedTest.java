package com.friday.nyc_schools_jpmc;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.TestCase.assertEquals;

import android.view.View;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.friday.nyc_schools_jpmc.dataSource.SchoolsProvider;
import com.friday.nyc_schools_jpmc.mock.MyBodyFactory;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import co.infinum.retromock.Retromock;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityScenarioRule<SchoolListActivity> activityRule =
            new ActivityScenarioRule<>(SchoolListActivity.class);

    private final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    private SchoolsProvider getProvider_mock() {
        return new Retromock.Builder()
                .retrofit(new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                )
                .defaultBodyFactory(new MyBodyFactory())
                .build()
                .create(SchoolsProvider.class);
    }

    private SchoolsProvider getProvider() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SchoolsProvider.class);
    }

    @Test
    public void schoolsList_isLoaded_mockData() {
        //check if network call for list loads the data which is not null
        try {
            assertEquals(getProvider_mock().getSchools().execute().body().get(0).getDbn(), "02M260");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void schoolsDetail_isLoaded_mockData() {
        //check if network call for details loads the data correctly
        try {
            assertEquals(getProvider_mock().getSchoolDetail("01M448").execute().body().get(0).getSchoolName(), "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void schoolsList_isLoaded() {
        //check if network call for list loads the data which is not null
        try {
            assertEquals(getProvider().getSchools().execute().body().get(0).getDbn(), "02M260");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void schoolsDetail_isLoaded() {
        //check if network call for details loads the data correctly
        try {
            assertEquals(getProvider().getSchoolDetail("01M448").execute().body().get(0).getSchoolName(), "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void List_Displayed() {
        onView(isRoot()).perform(waitTillVisible(3000));
        onView(withId(R.id.list)).check(matches(ViewMatchers.isDisplayed()));
    }


    //list item click and open details and check if school data matches
    @Test
    public void detail_data_on_click() {
        String SCHOOL_NAME_TEST = "Liberation Diploma Plus High School";
        onView(isRoot()).perform(waitTillVisible(3000)); //wait 5 sec for data to load
        onView(withId(R.id.list)).check(matches(ViewMatchers.isDisplayed()));
        onView(withId(R.id.list))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition(1, click()));
        onView(isRoot()).perform(waitTillVisible(3000));//wait 5 sec for data to load
        onView(withId(R.id.detail_name)).check(matches(isDisplayed()));
        onView(withId(R.id.detail_name)).check(matches(withText(SCHOOL_NAME_TEST)));
    }


    static ViewAction waitTillVisible(int timeout) {

        return new ViewAction() {
            @Override
            public String getDescription() {
                return "wait up to " + timeout + " milliseconds for the view to become visible";
            }

            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(timeout);
            }
        };
    }
}